
#include <iostream>
#include "f/f.h"
#include "d/d.h"

namespace f {
  void addIndentation(uint indentation) {
    for (uint i = 0; i < indentation; ++i) {
      std::cout << "  ";
    }
  }

  void doThingF(uint indentation) {
    addIndentation(indentation);
    std::cout << "F v1" << std::endl;

    d::doThingD(indentation + 1);
  };
}
